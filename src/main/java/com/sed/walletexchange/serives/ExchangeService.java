package com.sed.walletexchange.serives;

import com.sed.walletexchange.common.enums.CurrencyType;

public interface ExchangeService {
    Double exchange(Double amount, Integer inType,Integer outType);
}
