package com.sed.walletexchange.serives;

import com.sed.walletexchange.common.models.ResponseModel;
import com.sed.walletexchange.dal.models.User;

public interface UserService {
    ResponseModel<User> register(User user);
    ResponseModel<User> changeUserDetail(User user);
    ResponseModel<User> deleted(Long id);

    ResponseModel<User> getById(Long id);
    ResponseModel<User> getByWalletId(String walletId);
    ResponseModel<User> changeCurrencyType(Long id, Integer currencyType);

}
