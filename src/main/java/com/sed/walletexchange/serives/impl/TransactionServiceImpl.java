package com.sed.walletexchange.serives.impl;

import com.sed.walletexchange.common.ErrorMsg;
import com.sed.walletexchange.common.enums.TransactionType;
import com.sed.walletexchange.common.models.ResponseModel;
import com.sed.walletexchange.dal.DTO.TransactionDTO;
import com.sed.walletexchange.dal.models.Transaction;
import com.sed.walletexchange.dal.models.User;
import com.sed.walletexchange.dal.repositories.TransactionRepo;
import com.sed.walletexchange.dal.repositories.UserRepo;
import com.sed.walletexchange.serives.ExchangeService;
import com.sed.walletexchange.serives.TransactionService;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class TransactionServiceImpl implements TransactionService {

    TransactionRepo transactionRepo;
    UserRepo userRepo;
    ExchangeService exchangeService;

    @Autowired
    public TransactionServiceImpl(TransactionRepo transactionRepo, UserRepo userRepo, ExchangeService exchangeService) {
        this.transactionRepo = transactionRepo;
        this.userRepo = userRepo;
        this.exchangeService = exchangeService;
    }


    @Override
    @Transactional(Transactional.TxType.NEVER)
    public ResponseModel<TransactionDTO> register(TransactionDTO transactionDTO) {
        User outUser=userRepo.findById(transactionDTO.getUserId()).get();
        if (outUser==null){
            return new ResponseModel<>(transactionDTO, ErrorMsg.NOT_FOUND_USER.getCode(),ErrorMsg.NOT_FOUND_USER.getMsg());
        }
        if (outUser.getBalance()<transactionDTO.getAmount()){
            return new ResponseModel<>(transactionDTO, ErrorMsg.BALANCE_IS_LOW.getCode(),ErrorMsg.BALANCE_IS_LOW.getMsg());
        }
        User inUser=userRepo.findByWalletId(transactionDTO.getInWalletId());
        if (inUser==null){
            return new ResponseModel<>(transactionDTO, ErrorMsg.NOT_FOUND_USER.getCode(),ErrorMsg.NOT_FOUND_USER.getMsg());
        }
        Double exchangeAmount=exchangeService.exchange(transactionDTO.getAmount(),inUser.getCurrencyType(),outUser.getCurrencyType());
        Date transactionDate=new Date();
        String transactionId= UUID.randomUUID().toString().replace("-","");

        Transaction outTransaction=new Transaction();
        outTransaction.setAmount(transactionDTO.getAmount());
        outTransaction.setTransactionTypeEnum(TransactionType.OUT);
        outTransaction.setTransactionDate(transactionDate);
        outTransaction.setCurrencyType(outUser.getCurrencyType());
        outTransaction.setDescription(transactionDTO.getDescription());
        outTransaction.setTransactionId(transactionId);
        outTransaction.setUser(outUser);

        Transaction inTransaction=new Transaction();
        inTransaction.setAmount(exchangeAmount);
        inTransaction.setTransactionTypeEnum(TransactionType.IN);
        inTransaction.setTransactionDate(transactionDate);
        inTransaction.setCurrencyType(inUser.getCurrencyType());
        inTransaction.setDescription(transactionDTO.getDescription());
        inTransaction.setTransactionId(transactionId);
        inTransaction.setUser(inUser);

        Double oldOutBalance=outUser.getBalance();
        Double oldInBalance=inUser.getBalance();
        outUser.setBalance(oldOutBalance-transactionDTO.getAmount());
        inUser.setBalance(oldInBalance+exchangeAmount);

        userRepo.save(outUser);
        userRepo.save(inUser);
        transactionRepo.save(outTransaction);
        transactionRepo.save(inTransaction);

        transactionDTO.setTransactionId(transactionId);
        return new ResponseModel<TransactionDTO>(transactionDTO,ErrorMsg.SUCCESS.getCode(),ErrorMsg.SUCCESS.getMsg());
    }

    @Override
    public ResponseModel<List<Transaction>> getByUser(User user) {
        return null;
    }

    @Override
    public ResponseModel<TransactionDTO> chargeMyAccount(TransactionDTO transactionDTO) {
        User user=userRepo.findById(transactionDTO.getUserId()).get();
        if (user==null){
            return new ResponseModel<>(transactionDTO, ErrorMsg.NOT_FOUND_USER.getCode(),ErrorMsg.NOT_FOUND_USER.getMsg());
        }

        Date transactionDate=new Date();
        String transactionId= UUID.randomUUID().toString().replace("-","");

        Transaction outTransaction=new Transaction();
        outTransaction.setAmount(transactionDTO.getAmount());
        outTransaction.setTransactionTypeEnum(TransactionType.CHARGE);
        outTransaction.setTransactionDate(transactionDate);
        outTransaction.setCurrencyType(user.getCurrencyType());
        outTransaction.setDescription(transactionDTO.getDescription());
        outTransaction.setTransactionId(transactionId);
        outTransaction.setUser(user);

        Double oldBalance=user.getBalance();
        user.setBalance(oldBalance+transactionDTO.getAmount());
        transactionDTO.setTransactionId(transactionId);
        userRepo.save(user);
        transactionRepo.save(outTransaction);
        return new ResponseModel<TransactionDTO>(transactionDTO,ErrorMsg.SUCCESS.getCode(),ErrorMsg.SUCCESS.getMsg());
    }
}
