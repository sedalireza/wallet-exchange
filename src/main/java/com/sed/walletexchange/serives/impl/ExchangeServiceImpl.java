package com.sed.walletexchange.serives.impl;

import com.sed.walletexchange.common.enums.CurrencyType;
import com.sed.walletexchange.serives.ExchangeService;
import org.springframework.stereotype.Service;

@Service
public class ExchangeServiceImpl implements ExchangeService {
    @Override
    public Double exchange(Double amount, Integer inType,Integer outType) {
        if (inType==outType){
            return amount;
        }
        else if (outType==1){
            return amount/100000;
        }else {
            return amount*100000;
        }
    }
}
