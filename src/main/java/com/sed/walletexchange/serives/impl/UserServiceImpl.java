package com.sed.walletexchange.serives.impl;

import com.sed.walletexchange.common.ErrorMsg;
import com.sed.walletexchange.common.models.ResponseModel;
import com.sed.walletexchange.dal.models.User;
import com.sed.walletexchange.dal.repositories.UserRepo;
import com.sed.walletexchange.serives.ExchangeService;
import com.sed.walletexchange.serives.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    UserRepo userRepo;
    ExchangeService exchangeService;

    @Autowired
    public UserServiceImpl(UserRepo userRepo, ExchangeService exchangeService) {
        this.userRepo = userRepo;
        this.exchangeService = exchangeService;
    }



    @Override
    public ResponseModel<User> register(User user) {
        user.setInsertDate(new Date());
        user.setWalletId(UUID.randomUUID().toString().replace("-",""));
        try {
            return new ResponseModel(userRepo.save(user), ErrorMsg.SUCCESS.getCode(), ErrorMsg.SUCCESS.getMsg());
        } catch (Exception e) {
            return new ResponseModel(null, ErrorMsg.ERROR_REGISTER_USER.getCode(), ErrorMsg.ERROR_REGISTER_USER.getMsg());
        }

    }

    @Override
    public ResponseModel<User> changeUserDetail(User user) {
        userRepo.findById(user.getId());
        user.setUpdateDate(new Date());
        try {
            return new ResponseModel(userRepo.save(user), ErrorMsg.SUCCESS.getCode(), ErrorMsg.SUCCESS.getMsg());
        } catch (Exception e) {
            return new ResponseModel(null, ErrorMsg.ERROR_UPDATE_USER.getCode(), ErrorMsg.ERROR_UPDATE_USER.getMsg());
        }
    }

    @Override
    public ResponseModel<User> deleted(Long id) {
        try {
            User user = getById(id).getData();
            if (user != null) {
                user.setDeleted(true);
                return new ResponseModel(userRepo.save(user), ErrorMsg.SUCCESS.getCode(), ErrorMsg.SUCCESS.getMsg());
            }
            return new ResponseModel(null, ErrorMsg.NOT_FOUND_USER.getCode(), ErrorMsg.NOT_FOUND_USER.getMsg());
        } catch (Exception e) {
            return new ResponseModel(null, ErrorMsg.ERROR_UPDATE_USER.getCode(), ErrorMsg.ERROR_UPDATE_USER.getMsg());
        }

    }

    @Override
    public ResponseModel<User> getById(Long id) {
        try {
            return new ResponseModel(userRepo.findById(id).get(), ErrorMsg.SUCCESS.getCode(), ErrorMsg.SUCCESS.getMsg());
        } catch (Exception e) {
            return new ResponseModel(null, ErrorMsg.NOT_FOUND_USER.getCode(), ErrorMsg.NOT_FOUND_USER.getMsg());
        }

    }

    @Override
    public ResponseModel<User> getByWalletId(String walletId) {
        try {
            return new ResponseModel(userRepo.findByWalletId(walletId), ErrorMsg.SUCCESS.getCode(), ErrorMsg.SUCCESS.getMsg());
        } catch (Exception e) {
            return new ResponseModel(null, ErrorMsg.NOT_FOUND_USER.getCode(), ErrorMsg.NOT_FOUND_USER.getMsg());
        }
    }

    @Override
    public ResponseModel<User> changeCurrencyType(Long id, Integer currencyType) {
        try {
            User user=userRepo.findById(id).get();
            if (user==null){
                return new ResponseModel(null, ErrorMsg.NOT_FOUND_USER.getCode(), ErrorMsg.NOT_FOUND_USER.getMsg());
            }
            if (user.getCurrencyType()!=currencyType){
                Double newBalance=exchangeService.exchange(user.getBalance(),currencyType,user.getCurrencyType());
                user.setCurrencyType(currencyType);
                user.setBalance(newBalance);
                userRepo.save(user);
            }
            return new ResponseModel(user, ErrorMsg.SUCCESS.getCode(), ErrorMsg.SUCCESS.getMsg());

        }catch (Exception e){
            return new ResponseModel(null, ErrorMsg.NOT_FOUND_USER.getCode(), ErrorMsg.NOT_FOUND_USER.getMsg());
        }


    }

}
