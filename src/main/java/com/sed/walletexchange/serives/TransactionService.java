package com.sed.walletexchange.serives;

import com.sed.walletexchange.common.models.ResponseModel;
import com.sed.walletexchange.dal.DTO.TransactionDTO;
import com.sed.walletexchange.dal.models.Transaction;
import com.sed.walletexchange.dal.models.User;

import java.util.List;

public interface TransactionService {
    ResponseModel<TransactionDTO> register(TransactionDTO transactionDTO);
    ResponseModel<List<Transaction>> getByUser(User user);
    ResponseModel<TransactionDTO> chargeMyAccount (TransactionDTO transactionDTO);
}
