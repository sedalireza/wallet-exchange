package com.sed.walletexchange.dal.models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.sed.walletexchange.common.enums.CurrencyType;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "tbl_user")
public class User {
    @Expose
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Expose
    @Column(name = "first_name",length = 50)
    private String firstName;
    @Expose
    @Column(name = "last_name",length = 50)
    private String lastName;

    @Expose
    @Column(length = 50,unique = true,nullable = false)
    private String username;
    @Column(length = 50,nullable = false) //encrypt sha256 salt
    private String password;

    @Expose
    @Column(name = "mobile_number",length = 13)
    private String mobileNumber;
    @Expose
    @Column(length = 150,nullable = false)
    private String email;

    @Expose
    @Column(name = "currency_type")
    private Integer currencyType=1;
    @Expose
    private Double balance=0.0;
    @Expose
    @Column(name = "wallet_id",length = 32,unique = true,nullable = false)
    private String walletId;

    @Column(name = "insert_date",nullable = false)
    private Date insertDate;
    @Column(name = "update_date")
    private Date updateDate;
    @Column(name = "is_deleted")
    private Boolean isDeleted=false;

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setCurrencyType(Integer currencyType) {
        this.currencyType = currencyType;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyTypeEnum(CurrencyType currencyType) {
        this.currencyType = currencyType.getCode();
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }


    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
