package com.sed.walletexchange.dal.models;

import com.google.gson.Gson;
import com.sed.walletexchange.common.enums.CurrencyType;
import com.sed.walletexchange.common.enums.TransactionType;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "fk_user")
    private User user;
    @Column(name = "currency_type")
    private Integer currencyType;
    @Column(name = "amount")
    private Double amount;


    @Column(name = "transaction_date")
    private Date transactionDate;
    @Column(name = "transaction_id")
    private String transactionId;
    @Column(name = "transaction_type")
    private Integer transactionType;
    private String description;

    public Transaction(Long id) {
        this.id = id;
    }

    public Transaction() {
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return id.equals(that.id);
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyTypeEnum(CurrencyType currencyType) {
        this.currencyType = currencyType.getCode();
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getTransactionType() {
        return transactionType;
    }

    public void setTransactionTypeEnum(TransactionType transactionType) {
        this.transactionType = transactionType.getCode();
    }

    public void setCurrencyType(Integer currencyType) {
        this.currencyType = currencyType;
    }

    public void setTransactionType(Integer transactionType) {
        this.transactionType = transactionType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
