package com.sed.walletexchange.dal.repositories;

import com.sed.walletexchange.dal.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<User,Long> {
    @Query("select obj from User obj where obj.walletId = :walletId and obj.isDeleted<>1")
    User findByWalletId(String walletId);

}
