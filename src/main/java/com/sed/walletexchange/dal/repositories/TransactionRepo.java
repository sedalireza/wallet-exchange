package com.sed.walletexchange.dal.repositories;

import com.sed.walletexchange.dal.models.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepo extends CrudRepository<Transaction,Long> {

}
