package com.sed.walletexchange.dal.DTO;

public class TransactionDTO {
    private Long userId;
    private String inWalletId;
    private Double amount;
    private String description;
    private String transactionId;

    public Long getUserId() {
        return userId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getInWalletId() {
        return inWalletId;
    }

    public void setInWalletId(String inWalletId) {
        this.inWalletId = inWalletId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
