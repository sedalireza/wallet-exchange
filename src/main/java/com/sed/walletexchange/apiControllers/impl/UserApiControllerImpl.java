package com.sed.walletexchange.apiControllers.impl;

import com.sed.walletexchange.apiControllers.UserApiController;
import com.sed.walletexchange.common.ErrorMsg;
import com.sed.walletexchange.common.models.ResponseModel;
import com.sed.walletexchange.dal.models.User;
import com.sed.walletexchange.serives.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/user")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "عملیات با موفقیت انجام شد"),
        @ApiResponse(code = 400, message = "خطا در انجام عملیات"),
        @ApiResponse(code = 300, message = "کاربر یافت نشد"),
        @ApiResponse(code = 302, message = "خدا در درج اطلاعت کاربر ( داده تکراری یا ناقص)"),
        @ApiResponse(code = 303, message = "خطا در زمان تغییر اطلاعات کاربر")})
public class UserApiControllerImpl implements UserApiController {

    UserService userService;

    @Autowired
    public UserApiControllerImpl(UserService userService) {
        this.userService = userService;
    }




    @Override
    @PostMapping(path = "/register")
    @ApiOperation(value="اضافه کردن کاربر")
    public ResponseEntity<ResponseModel<User>> registerUser(@RequestBody User user) {
        try {
            ResponseModel responseModel=userService.register(user);
            return new ResponseEntity(responseModel, HttpStatus.valueOf(responseModel.getCode()));
        }catch (Exception e){
            return new ResponseEntity(new ResponseModel<User>(null, ErrorMsg.ERROR.getCode(),ErrorMsg.ERROR.getMsg()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    @PostMapping(path = "/update")
    @ApiOperation(value="تغییر اطلاعات کاربر")
    public ResponseEntity<ResponseModel<User>> updateUser(@RequestBody User user) {
        try {
            ResponseModel responseModel=userService.changeUserDetail(user);
            return new ResponseEntity(responseModel, HttpStatus.valueOf(responseModel.getCode()));
        }catch (Exception e){
            return new ResponseEntity(new ResponseModel<User>(null, ErrorMsg.ERROR.getCode(),ErrorMsg.ERROR.getMsg()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    @DeleteMapping(path = "/remove/{id}")
    @ApiOperation(value="حذف کاربر")
    public ResponseEntity<ResponseModel<User>> removeUser(@PathVariable("id") Long id) {
        try {
            ResponseModel responseModel=userService.deleted(id);
            return new ResponseEntity(responseModel, HttpStatus.valueOf(responseModel.getCode()));
        }catch (Exception e){
            return new ResponseEntity(new ResponseModel<User>(null, ErrorMsg.ERROR.getCode(),ErrorMsg.ERROR.getMsg()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    @GetMapping(path = "/get/{id}")
    @ApiOperation(value="جستجوی کاربر با id")
    public ResponseEntity<ResponseModel<User>> findUserById(@PathVariable("id") Long id) {
        try {
            ResponseModel responseModel=userService.getById(id);
            return new ResponseEntity(responseModel, HttpStatus.valueOf(responseModel.getCode()));
        }catch (Exception e){
            return new ResponseEntity(new ResponseModel<User>(null, ErrorMsg.ERROR.getCode(),ErrorMsg.ERROR.getMsg()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    @GetMapping(path = "/getByWallet/{walletId}")
    @ApiOperation(value="جستجوی کاربر با شناسه کیف پول")
    public ResponseEntity<ResponseModel<User>> findUserByWalletId(@PathVariable("walletId")String walletId) {
        try {
            ResponseModel responseModel=userService.getByWalletId(walletId);
            return new ResponseEntity(responseModel, HttpStatus.valueOf(responseModel.getCode()));
        }catch (Exception e){
            return new ResponseEntity(new ResponseModel<User>(null, ErrorMsg.ERROR.getCode(),ErrorMsg.ERROR.getMsg()),HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    @GetMapping(path = "/changeCurrency/{id}/{currencyType}")
    @ApiOperation(value="تغییر نوع ارز کیف پول")
    public ResponseEntity<ResponseModel<User>> changeCurrencyType(@PathVariable("id") Long id,@PathVariable("currencyType") Integer currencyType) {
        try {
            ResponseModel responseModel=userService.changeCurrencyType(id,currencyType);
            return new ResponseEntity(responseModel, HttpStatus.valueOf(responseModel.getCode()));
        }catch (Exception e){
            return new ResponseEntity(new ResponseModel<User>(null, ErrorMsg.ERROR.getCode(),ErrorMsg.ERROR.getMsg()),HttpStatus.EXPECTATION_FAILED);
        }
    }

}
