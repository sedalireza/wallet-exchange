package com.sed.walletexchange.apiControllers.impl;

import com.sed.walletexchange.apiControllers.TransactionApiController;
import com.sed.walletexchange.common.ErrorMsg;
import com.sed.walletexchange.common.models.ResponseModel;
import com.sed.walletexchange.dal.DTO.TransactionDTO;
import com.sed.walletexchange.dal.models.Transaction;
import com.sed.walletexchange.serives.TransactionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/transaction")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "عملیات با موفقیت انجام شد"),
        @ApiResponse(code = 400, message = "خطا در انجام عملیات"),
        @ApiResponse(code = 300, message = "کاربر یافت نشد"),
        @ApiResponse(code = 301, message = "موجودی کاربر کافی نیست")})
public class TransactionApiContrallerImpl implements TransactionApiController {

    TransactionService transactionService;

    @Autowired
    public TransactionApiContrallerImpl(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    @PostMapping(path = "/createTransaction")
    @ApiOperation(value = "عملیات تراکنش بین کاربر")
    public ResponseEntity<ResponseModel<TransactionDTO>> createTransaction(@RequestBody TransactionDTO transactionDTO) {
        try{
            ResponseModel responseModel=transactionService.register(transactionDTO);
            return new ResponseEntity(responseModel, HttpStatus.valueOf(responseModel.getCode()));
        }catch (Exception e){
            return new ResponseEntity(new ResponseModel<>(transactionDTO, ErrorMsg.ERROR.getCode(),ErrorMsg.ERROR.getMsg()), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    @PostMapping(path = "/chargeMyWallet")
    @ApiOperation(value = "افزایش اعتبار کیف پول")
    public ResponseEntity<ResponseModel<TransactionDTO>> chargeMyAccount(@RequestBody TransactionDTO transactionDTO) {
        try{
            ResponseModel responseModel=transactionService.chargeMyAccount(transactionDTO);
            return new ResponseEntity(responseModel, HttpStatus.valueOf(responseModel.getCode()));
        }catch (Exception e){
            return new ResponseEntity(new ResponseModel<>(transactionDTO, ErrorMsg.ERROR.getCode(),ErrorMsg.ERROR.getMsg()), HttpStatus.EXPECTATION_FAILED);
        }
    }


}
