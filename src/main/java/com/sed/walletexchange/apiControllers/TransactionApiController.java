package com.sed.walletexchange.apiControllers;

import com.sed.walletexchange.common.models.ResponseModel;
import com.sed.walletexchange.dal.DTO.TransactionDTO;
import com.sed.walletexchange.dal.models.Transaction;
import org.springframework.http.ResponseEntity;

public interface TransactionApiController {
    ResponseEntity<ResponseModel<TransactionDTO>> createTransaction(TransactionDTO transactionDTO);
    ResponseEntity<ResponseModel<TransactionDTO>> chargeMyAccount(TransactionDTO transactionDTO);

}
