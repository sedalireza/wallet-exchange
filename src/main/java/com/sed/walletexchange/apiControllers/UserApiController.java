package com.sed.walletexchange.apiControllers;

import com.sed.walletexchange.common.enums.CurrencyType;
import com.sed.walletexchange.common.models.ResponseModel;
import com.sed.walletexchange.dal.models.User;
import org.springframework.http.ResponseEntity;

public interface UserApiController {
    ResponseEntity<ResponseModel<User>> registerUser(User user);
    ResponseEntity<ResponseModel<User>> updateUser(User user);
    ResponseEntity<ResponseModel<User>> removeUser(Long id);

    ResponseEntity<ResponseModel<User>> findUserById(Long id);
    ResponseEntity<ResponseModel<User>> findUserByWalletId(String walletId);

    ResponseEntity<ResponseModel<User>> changeCurrencyType(Long id, Integer currencyType);



}
