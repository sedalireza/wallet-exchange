package com.sed.walletexchange.common;

public enum ErrorMsg {
    SUCCESS(200,"success"),
    ERROR(400,"error"),
    NOT_FOUND_USER(300,"user not found"),
    BALANCE_IS_LOW(301,"user balance is low"),
    ERROR_REGISTER_USER(302,"erro when register user"),
    ERROR_UPDATE_USER(303,"error when update user");

    private Integer code;
    private String msg;

    ErrorMsg(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
