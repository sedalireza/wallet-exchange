package com.sed.walletexchange.common.enums;

public enum TransactionType {
    OUT(1),
    IN(2),
    CHARGE(3);
    private Integer code;

    TransactionType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
