package com.sed.walletexchange.common.enums;

public enum CurrencyType {
    RIAL(1),
    DOLLAR(2);

    private Integer code;

    CurrencyType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
